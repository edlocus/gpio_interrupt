#include "config.h"

#include "TM4C123GH6PM.h"
#include "stdint.h"

char button_flag;

//#define MACRO_LIB
#define CMSIS_LIB


#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80

#define RED_LED     BIT1
#define BLUE_LED    BIT2
#define GREEN_LED   BIT3

/**
 * main.c
 */

#ifdef MACRO_LIB

int main(void)
{

    RCGCGPIO_1 |= BIT5;  // clocking gating to enable port F

    GPIOLOCK_PORT_F = 0x4C4F434B;  // unlock by key to modify commit register
    GPIOCR_PORT_F |= 0x1F;  // set commit register
    GPIODEN_PORT_F |= 0x1F;  // enable digital enable

    /*** Set PF0 direction as output ***/
    GPIODIR_PORT_F |= 0x0E;  // Select selected gpio direction as output

    /*** init button config PF0 and PF4 direction register as input ***/
    GPIODIR_PORT_F &= ~(BIT0);

    GPIOPUR_PORT_F |= BIT0 + BIT4;  // enable pull-up on PF0 and PF4

    GPIOIS_PORT_F &= ~BIT0;  // BIT0 enables edge detection

    GPIOIBE_PORT_F &= ~BIT0;  // by clearing edge detection taken care by IEV

    GPIOIEV_PORT_F &= ~BIT0;  // by clearing interrupt detects on falling edge

    GPIOIM_PORT_F |= BIT0;  // GPIO Interrupt enable by setting interrupt sent to interrupt controller

    INT_0_31_ENABLE |= (1<<30);  // enable interrupt for GPIO port F

    while(1)
    {
        if(button_flag == 1)  // button flag will get set when button interrupt occurs
        {
            GPIODATA_PORT_F ^= RED_LED;
            button_flag = 0;
        }
    }
}

#endif

#ifdef CMSIS_LIB

int main(void)
{

    RCGCGPIO_1 |= BIT5;  // clocking gating to enable port F

    GPIOLOCK_PORT_F = 0x4C4F434B;  // unlock by key to modify commit register
    GPIOCR_PORT_F |= 0x1F;  // set commit register
    GPIODEN_PORT_F |= 0x1F;  // enable digital enable

    /*** Set PF0 direction as output ***/
    GPIODIR_PORT_F |= 0x0E;  // Select selected gpio direction as output

    /*** init button config PF0 and PF4 direction register as input ***/
    GPIODIR_PORT_F &= ~(BIT0);

    GPIOPUR_PORT_F |= BIT0 + BIT4;  // enable pull-up on PF0 and PF4

    GPIOIS_PORT_F &= ~BIT0;  // BIT0 enables edge detection

    GPIOIBE_PORT_F &= ~BIT0;  // by clearing edge detection taken care by IEV

    GPIOIEV_PORT_F &= ~BIT0;  // by clearing interrupt detects on falling edge

    GPIOIM_PORT_F |= BIT0;  // GPIO Interrupt enable by setting interrupt sent to interrupt controller

    INT_0_31_ENABLE |= (1<<30);  // enable interrupt for GPIO port F

    while(1)
    {
        if(button_flag == 1)  // button flag will get set when button interrupt occurs
        {
            GPIODATA_PORT_F ^= RED_LED;
            button_flag = 0;
        }
    }
}

#endif
